/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct {
	uint8_t DisplayControl;
	uint8_t DisplayFunction;
	uint8_t DisplayMode;
	uint8_t Rows;
	uint8_t Cols;
	uint8_t currentX;
	uint8_t currentY;
} HD44780_Options;

static HD44780_Options HD44780_Opts = { 0 };
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;
DMA_HandleTypeDef hdma_tim5_ch2;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;
DMA_HandleTypeDef hdma_usart2_tx;

osThreadId defaultTaskHandle;
osThreadId tempTaskHandle;
osThreadId distTaskHandle;
osThreadId ledTaskHandle;
osThreadId buttonTaskHandle;
osThreadId statTaskHandle;
osMessageQId tempQueueHandle;
osMessageQId distQueueHandle;
osSemaphoreId uart2SemHandle;
/* USER CODE BEGIN PV */
float temperatureGlobal = 0.0;
uint32_t distanceGlobal = 0;
uint32_t ledPowerGlobal = 0;
uint32_t buttonGlobal = 0;
uint32_t triggerGlobal = 0;
uint32_t statGlobal = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART1_UART_Init(void);
void StartDefaultTask(void const * argument);
void tempTaskFunc(void const * argument);
void distTaskFunc(void const * argument);
void ledTaskFunc(void const * argument);
void buttonTaskFunc(void const * argument);
void statTaskFunc(void const * argument);

/* USER CODE BEGIN PFP */
void LCD_4bit(uint8_t cmd);
void LCD_Cmd(uint8_t cmd);
void LCD_Data(uint8_t data);
void LCD_CursorSet(uint8_t col, uint8_t row);
void LCD_Clear(void);
void LCD_DisplayOn(void);
void LCD_DisplayOff(void);
void LCD_BlinkOn(void);
void LCD_BlinkOff(void);
void LCD_CursorOn(void);
void LCD_CursorOff(void);
void LCD_CreateCustom(uint8_t location, uint8_t *data);
void LCD_PrintCustom(uint8_t x, uint8_t y, uint8_t location);
void LCD_Print(uint8_t x, uint8_t y, char* str);
void LCD_Init(uint8_t cols, uint8_t rows);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void LCD_4bit(uint8_t cmd) {

	/* Set output port */
	HAL_GPIO_WritePin(D7_GPIO_Port, D7_Pin, (cmd & 0x08));
	HAL_GPIO_WritePin(D6_GPIO_Port, D6_Pin, (cmd & 0x04));
	HAL_GPIO_WritePin(D5_GPIO_Port, D5_Pin, (cmd & 0x02));
	HAL_GPIO_WritePin(D4_GPIO_Port, D4_Pin, (cmd & 0x01));

	HAL_GPIO_WritePin(E_GPIO_Port, E_Pin, GPIO_PIN_SET);
	HAL_Delay(1);
	HAL_GPIO_WritePin(E_GPIO_Port, E_Pin, GPIO_PIN_RESET);
	HAL_Delay(1);
}

void LCD_Cmd(uint8_t cmd) {
	/* Command mode */
	HAL_GPIO_WritePin(RS_GPIO_Port, RS_Pin, GPIO_PIN_RESET);

	/* High nibble */
	LCD_4bit(cmd >> 4);
	/* Low nibble */
	LCD_4bit(cmd & 0x0F);
}

void LCD_Data(uint8_t data) {
	/* Data mode */
	HAL_GPIO_WritePin(RS_GPIO_Port, RS_Pin, GPIO_PIN_SET);

	/* High nibble */
	LCD_4bit(data >> 4);
	/* Low nibble */
	LCD_4bit(data & 0x0F);
}

void LCD_CursorSet(uint8_t col, uint8_t row) {
	uint8_t row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };

	/* Go to beginning */
	if (row >= HD44780_Opts.Rows)
		row = 0;

	/* Set current column and row */
	HD44780_Opts.currentX = col;
	HD44780_Opts.currentY = row;

	/* Set location address */
	LCD_Cmd(HD44780_SETDDRAMADDR | (col + row_offsets[row]));
}

void LCD_Clear(void) {
	LCD_Cmd(HD44780_CLEARDISPLAY);
	HAL_Delay(3);
}

void LCD_DisplayOn(void) {
	HD44780_Opts.DisplayControl |= HD44780_DISPLAYON;
	LCD_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void LCD_DisplayOff(void) {
	HD44780_Opts.DisplayControl &= ~HD44780_DISPLAYON;
	LCD_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void LCD_BlinkOn(void) {
	HD44780_Opts.DisplayControl |= HD44780_BLINKON;
	LCD_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void LCD_BlinkOff(void) {
	HD44780_Opts.DisplayControl &= ~HD44780_BLINKON;
	LCD_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void LCD_CursorOn(void) {
	HD44780_Opts.DisplayControl |= HD44780_CURSORON;
	LCD_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void LCD_CursorOff(void) {
	HD44780_Opts.DisplayControl &= ~HD44780_CURSORON;
	LCD_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void LCD_CreateCustom(uint8_t location, uint8_t *data) {
	uint8_t i;
	/* We have 8 locations available for custom characters */
	location &= 0x07;
	LCD_Cmd(HD44780_SETCGRAMADDR | (location << 3));

	for (i = 0; i < 8; i++)
		LCD_Data(data[i]);
}

void LCD_PrintCustom(uint8_t x, uint8_t y, uint8_t location) {
	LCD_CursorSet(x, y);
	LCD_Data(location);
}

void LCD_Print(uint8_t x, uint8_t y, char* str) {
	LCD_CursorSet(x, y);
	while (*str) {
		if (HD44780_Opts.currentX >= HD44780_Opts.Cols) {
			HD44780_Opts.currentX = 0;
			HD44780_Opts.currentY++;
			LCD_CursorSet(HD44780_Opts.currentX, HD44780_Opts.currentY);
		}
		if (*str == '\n') {
			HD44780_Opts.currentY++;
			LCD_CursorSet(HD44780_Opts.currentX, HD44780_Opts.currentY);
		} else if (*str == '\r') {
			LCD_CursorSet(0, HD44780_Opts.currentY);
		} else {
			LCD_Data(*str);
			HD44780_Opts.currentX++;
		}
		str++;
	}
}

void LCD_Init(uint8_t cols, uint8_t rows) {
	int i = 0;

	/* Set LCD width and height */
	HD44780_Opts.Rows = rows;
	HD44780_Opts.Cols = cols;

	/* Try to set 4bit mode */
	for (i = 0; i < 2; i++) {
		LCD_4bit(0x03);
		HAL_Delay(5);
	}

	/* Set 4-bit interface */
	LCD_4bit(0x02);
	HAL_Delay(1);

	/* Set # lines, font size, etc. */
	HD44780_Opts.DisplayFunction = HD44780_4BITMODE | HD44780_5x8DOTS;
	if (rows > 1)
		HD44780_Opts.DisplayFunction |= HD44780_2LINE;
	else
		HD44780_Opts.DisplayFunction |= HD44780_1LINE;
	LCD_Cmd(HD44780_FUNCTIONSET | HD44780_Opts.DisplayFunction);

	/* Default font directions */
	HD44780_Opts.DisplayMode = HD44780_ENTRYLEFT | HD44780_ENTRYSHIFTDECREMENT;
	LCD_Cmd(HD44780_ENTRYMODESET | HD44780_Opts.DisplayMode);

	/* Turn the display on with no cursor or blinking default */
	LCD_DisplayOn();

	/* Clear lcd */
	LCD_Cmd(HD44780_CLEARDISPLAY);
	HAL_Delay(3);

}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_USART2_UART_Init();
	MX_ADC1_Init();
	MX_TIM4_Init();
	MX_TIM5_Init();
	MX_TIM2_Init();
	MX_USART1_UART_Init();
	/* USER CODE BEGIN 2 */
	LCD_Init(16, 2);
	LCD_Print(0, 0, "Nucleo-F446RE");
	LCD_Print(0, 1, "Piotr Kasierski");

	/* USER CODE END 2 */

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* Create the semaphores(s) */
	/* definition and creation of uart2Sem */
	osSemaphoreDef(uart2Sem);
	uart2SemHandle = osSemaphoreCreate(osSemaphore(uart2Sem), 1);

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* Create the queue(s) */
	/* definition and creation of tempQueue */
	osMessageQDef(tempQueue, 16, uint32_t);
	tempQueueHandle = osMessageCreate(osMessageQ(tempQueue), NULL);

	/* definition and creation of distQueue */
	osMessageQDef(distQueue, 16, uint32_t);
	distQueueHandle = osMessageCreate(osMessageQ(distQueue), NULL);

	/* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
	/* USER CODE END RTOS_QUEUES */

	/* Create the thread(s) */
	/* definition and creation of defaultTask */
	osThreadDef(defaultTask, StartDefaultTask, osPriorityBelowNormal, 0, 128);
	defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

	/* definition and creation of tempTask */
	osThreadDef(tempTask, tempTaskFunc, osPriorityNormal, 0, 128);
	tempTaskHandle = osThreadCreate(osThread(tempTask), NULL);

	/* definition and creation of distTask */
	osThreadDef(distTask, distTaskFunc, osPriorityHigh, 0, 128);
	distTaskHandle = osThreadCreate(osThread(distTask), NULL);

	/* definition and creation of ledTask */
	osThreadDef(ledTask, ledTaskFunc, osPriorityAboveNormal, 0, 128);
	ledTaskHandle = osThreadCreate(osThread(ledTask), NULL);

	/* definition and creation of buttonTask */
	osThreadDef(buttonTask, buttonTaskFunc, osPriorityRealtime, 0, 128);
	buttonTaskHandle = osThreadCreate(osThread(buttonTask), NULL);

	/* definition and creation of statTask */
	osThreadDef(statTask, statTaskFunc, osPriorityRealtime, 0, 128);
	statTaskHandle = osThreadCreate(osThread(statTask), NULL);

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

	/* Start scheduler */
	osKernelStart();

	/* We should never get here as control is now taken by the scheduler */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE()
	;
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 180;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 2;
	RCC_OscInitStruct.PLL.PLLR = 2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Activate the Over-Drive mode
	 */
	if (HAL_PWREx_EnableOverDrive() != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief ADC1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_ADC1_Init(void) {

	/* USER CODE BEGIN ADC1_Init 0 */

	/* USER CODE END ADC1_Init 0 */

	ADC_ChannelConfTypeDef sConfig = { 0 };

	/* USER CODE BEGIN ADC1_Init 1 */

	/* USER CODE END ADC1_Init 1 */
	/** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
	 */
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV8;
	hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	hadc1.Init.ScanConvMode = ENABLE;
	hadc1.Init.ContinuousConvMode = DISABLE;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.NbrOfConversion = 2;
	hadc1.Init.DMAContinuousRequests = DISABLE;
	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	if (HAL_ADC_Init(&hadc1) != HAL_OK) {
		Error_Handler();
	}
	/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
	 */
	sConfig.Channel = ADC_CHANNEL_VREFINT;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
		Error_Handler();
	}
	/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
	 */
	sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
	sConfig.Rank = 2;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN ADC1_Init 2 */

	/* USER CODE END ADC1_Init 2 */

}

/**
 * @brief TIM2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM2_Init(void) {

	/* USER CODE BEGIN TIM2_Init 0 */

	/* USER CODE END TIM2_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
	TIM_MasterConfigTypeDef sMasterConfig = { 0 };
	TIM_OC_InitTypeDef sConfigOC = { 0 };

	/* USER CODE BEGIN TIM2_Init 1 */

	/* USER CODE END TIM2_Init 1 */
	htim2.Instance = TIM2;
	htim2.Init.Prescaler = 0;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = 0;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim2) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_TIM_PWM_Init(&htim2) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1)
			!= HAL_OK) {
		Error_Handler();
	}
	if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2)
			!= HAL_OK) {
		Error_Handler();
	}
	if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4)
			!= HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM2_Init 2 */

	/* USER CODE END TIM2_Init 2 */
	HAL_TIM_MspPostInit(&htim2);

}

/**
 * @brief TIM4 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM4_Init(void) {

	/* USER CODE BEGIN TIM4_Init 0 */

	/* USER CODE END TIM4_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
	TIM_MasterConfigTypeDef sMasterConfig = { 0 };
	TIM_OC_InitTypeDef sConfigOC = { 0 };

	/* USER CODE BEGIN TIM4_Init 1 */

	/* USER CODE END TIM4_Init 1 */
	htim4.Instance = TIM4;
	htim4.Init.Prescaler = 0;
	htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim4.Init.Period = 65535;
	htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim4) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_TIM_PWM_Init(&htim4) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2)
			!= HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM4_Init 2 */

	/* USER CODE END TIM4_Init 2 */
	HAL_TIM_MspPostInit(&htim4);

}

/**
 * @brief TIM5 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM5_Init(void) {

	/* USER CODE BEGIN TIM5_Init 0 */

	/* USER CODE END TIM5_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
	TIM_MasterConfigTypeDef sMasterConfig = { 0 };
	TIM_IC_InitTypeDef sConfigIC = { 0 };

	/* USER CODE BEGIN TIM5_Init 1 */

	/* USER CODE END TIM5_Init 1 */
	htim5.Instance = TIM5;
	htim5.Init.Prescaler = 0;
	htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim5.Init.Period = 4294967295;
	htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim5) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_TIM_IC_Init(&htim5) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_BOTHEDGE;
	sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
	sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
	sConfigIC.ICFilter = 15;
	if (HAL_TIM_IC_ConfigChannel(&htim5, &sConfigIC, TIM_CHANNEL_2) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM5_Init 2 */

	/* USER CODE END TIM5_Init 2 */

}

/**
 * @brief USART1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART1_UART_Init(void) {

	/* USER CODE BEGIN USART1_Init 0 */

	/* USER CODE END USART1_Init 0 */

	/* USER CODE BEGIN USART1_Init 1 */

	/* USER CODE END USART1_Init 1 */
	huart1.Instance = USART1;
	huart1.Init.BaudRate = 9600;
	huart1.Init.WordLength = UART_WORDLENGTH_8B;
	huart1.Init.StopBits = UART_STOPBITS_1;
	huart1.Init.Parity = UART_PARITY_NONE;
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN USART1_Init 2 */

	/* USER CODE END USART1_Init 2 */

}

/**
 * @brief USART2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART2_UART_Init(void) {

	/* USER CODE BEGIN USART2_Init 0 */

	/* USER CODE END USART2_Init 0 */

	/* USER CODE BEGIN USART2_Init 1 */

	/* USER CODE END USART2_Init 1 */
	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart2) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN USART2_Init 2 */

	/* USER CODE END USART2_Init 2 */

}

/**
 * Enable DMA controller clock
 */
static void MX_DMA_Init(void) {

	/* DMA controller clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE()
	;
	__HAL_RCC_DMA2_CLK_ENABLE()
	;

	/* DMA interrupt init */
	/* DMA1_Stream4_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Stream4_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA1_Stream4_IRQn);
	/* DMA1_Stream6_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Stream6_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA1_Stream6_IRQn);
	/* DMA2_Stream0_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
	/* DMA2_Stream2_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Stream2_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);
	/* DMA2_Stream7_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE()
	;
	__HAL_RCC_GPIOH_CLK_ENABLE()
	;
	__HAL_RCC_GPIOA_CLK_ENABLE()
	;
	__HAL_RCC_GPIOB_CLK_ENABLE()
	;

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, D6_Pin | D5_Pin | D4_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC, E_Pin | GPIO_PIN_8 | GPIO_PIN_9, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, D7_Pin | RS_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin : B1_Pin */
	GPIO_InitStruct.Pin = B1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : D6_Pin D5_Pin D4_Pin */
	GPIO_InitStruct.Pin = D6_Pin | D5_Pin | D4_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : E_Pin */
	GPIO_InitStruct.Pin = E_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(E_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PC8 PC9 */
	GPIO_InitStruct.Pin = GPIO_PIN_8 | GPIO_PIN_9;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pins : D7_Pin RS_Pin */
	GPIO_InitStruct.Pin = D7_Pin | RS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */
/* Callback z przerwania po wysylce przez UART */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef * husart) {
	if (husart->Instance == USART2) {
		osSemaphoreRelease(uart2SemHandle); /* Zwalnia semafor po wysylce danych */
	}
}
/* Callback z przerwania po skonczeniu odczytu ADC */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
	if (hadc->Instance == ADC1) {
		osSignalSet(tempTaskHandle, ADC1_END);
	}
}
/* Callback z przerwania po przycisku */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	if (GPIO_Pin == B1_Pin) {
		HAL_NVIC_DisableIRQ(EXTI15_10_IRQn); /* Wylacza przerwania przycisku */
		osSignalSet(tempTaskHandle, BUTTON_PUSH);
	}
}
/* Callback z przerwania nadaniu sygnalułu PWM trigger*/
void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == TIM4) {
		HAL_TIM_PWM_Stop_IT(&htim4, TIM_CHANNEL_2); /* Zatrzymanie generowania sygnału trigger */
	}
}
/* Callback z przerwania po odebraniu echa */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == TIM5) {
		osSignalSet(distTaskHandle, ECHO_SIGNAL); /* Notifikacja do tasku */
	}
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
 * @brief  Function implementing the defaultTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument) {

	/* USER CODE BEGIN 5 */
	char text[20] = { 0 };
	uint32_t number = 0;
	osEvent eventMessage = { 0 };

	/* Infinite loop */
	for (;;) {
		eventMessage = osMessageGet(tempQueueHandle, osWaitForever);
		if (osEventMessage == eventMessage.status) {
			number = eventMessage.value.v;
			snprintf(text, sizeof(text), "Temp: %lu.%lu\n\r", number / 100,
					number % 100);
			if (osOK == osSemaphoreWait(uart2SemHandle, osWaitForever))
				HAL_UART_Transmit_DMA(&huart2, (uint8_t *) text, strlen(text));

		}
	}
	/* USER CODE END 5 */
}

/* USER CODE BEGIN Header_tempTaskFunc */
/**
 * @brief Function implementing the tempTask thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_tempTaskFunc */
void tempTaskFunc(void const * argument) {
	/* USER CODE BEGIN tempTaskFunc */
	osEvent buttonSignal = { 0 };
	osEvent adc1Signal = { 0 };
	uint32_t number = 0;
	uint32_t adc1Read[2] = { 0 };
	uint32_t *vrRead = &adc1Read[0]; //odczyt napiecia referencyjnego
	uint32_t *tsRead = &adc1Read[1]; //odczyt napiecia czujnika temperatury
	uint16_t *pVrefintCal = (uint16_t *) 0x1FFF7A2A; //Wskaznik na dane kalibracji napiecie referencyjne 3.3
	uint16_t *pAdcRef30 = (uint16_t *) 0x1FFF7A2C; //Wskaznik na dane kalibracji temperatura 30
	uint16_t *pAdcRef110 = (uint16_t *) 0x1FFF7A2E; //Wskaznik na dane kalibracji temperatura 110
	float v30 = (*pAdcRef30 * 3.3) / 4095; //wylicza napiecia dla temperatury 30
	float v110 = (*pAdcRef110 * 3.3) / 4095; //wylicza napiecia dla temperatury 110
	float vtsDev = (v30 - v110) / 80; //wylicza odchylenie sensora
	float vdda = 0.0; //Wyliczone napiecie vdda
	float vts = 0.0; //Wyliczone napiecie z sensora temperatury
	float temperature = 0.0; //Wyliczona temperatura procesora

	/* Infinite loop */
	for (;;) {
		buttonSignal = osSignalWait(BUTTON_PUSH, osWaitForever);
		if (osEventSignal == buttonSignal.status
				&& BUTTON_PUSH == buttonSignal.value.signals) {
			osThreadResume(buttonTaskHandle);

			HAL_ADC_Start_DMA(&hadc1, adc1Read, 2);

			adc1Signal = osSignalWait(ADC1_END, osWaitForever);
			if (osEventSignal == adc1Signal.status
					&& ADC1_END == adc1Signal.value.signals) {

				vdda = (3.3 * (*pVrefintCal)) / *vrRead; //wyliczna napiecie vdda na podstawie danych pomiaru napiecia referencyjnego
				vts = (vdda / 4095) * *tsRead; //wylicza napiecie uzyskane na sensorze temperatury
				temperature = ((v30 - vts) / vtsDev) + 30; //oblicza temperature na podstawie pomiaru vref i danych referencyjnych vtemp

				temperatureGlobal = temperature;
				number = temperature * 100.0;
				osMessagePut(tempQueueHandle, number,
				osWaitForever);
			}
			HAL_ADC_Stop_DMA(&hadc1);
		}
	}
	/* USER CODE END tempTaskFunc */
}

/* USER CODE BEGIN Header_distTaskFunc */
/**
 * @brief Function implementing the distTask thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_distTaskFunc */
void distTaskFunc(void const * argument) {
	/* USER CODE BEGIN distTaskFunc */
	uint32_t echoRead[2] = { 0 }; /* Tablica do odczytu dla DMA */
	uint32_t echoTicks = 0; /* Ilosc ticków echa */
	uint32_t triggerTicks = (((SystemCoreClock / 2) / 1000000) * 10); /* Ilosc tickow timera dla sygnalu rozpoczynajacego pomiar */
	uint32_t ticksUs = SystemCoreClock / 1000000;
	osEvent echoSignal = { 0 };
	uint32_t distance = 0;

	__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_2, triggerTicks); /* Ustawienie wypelnienia sygnału PWM (Duty) */
	/* Infinite loop */
	for (;;) {
		__HAL_TIM_SET_COUNTER(&htim5, 0); /* Wyzerowanie licznika timera */
		HAL_TIM_IC_Start_DMA(&htim5, TIM_CHANNEL_2, echoRead, 2); /* Start timera IC w trybie nieblokującym z wykorzystaniem DMA - Echo czujnika */
		__HAL_TIM_SET_COUNTER(&htim4, 0); /* Wyzerowanie licznika timera */
		HAL_TIM_PWM_Start_IT(&htim4, TIM_CHANNEL_2); /* Start sygnalu PWM - Trigger czujnika 10ns */

		echoSignal = osSignalWait(ECHO_SIGNAL, 250); /* Czeka 250 ms na powrot sygnalu echa */
		if (osEventSignal == echoSignal.status
				&& ECHO_SIGNAL == echoSignal.value.signals) {

			triggerGlobal++;
			echoTicks = echoRead[1] - echoRead[0]; /* Wyliczanie tickow echa */
			distance = ((echoTicks / ticksUs) * 34) / 100; /* Wylicza dystans w mm */

			distanceGlobal = distance; /* Przypisuje wartosc do zmiennej globalnej dla podgladu */
			osMessagePut(distQueueHandle, distance, 0); /* Wysyła wartosc do taska sterujacego dioda */
		}
		HAL_TIM_IC_Stop_DMA(&htim5, TIM_CHANNEL_2); /* Wylacza IC */
		osDelay(15);
	}
	/* USER CODE END distTaskFunc */
}

/* USER CODE BEGIN Header_ledTaskFunc */
/**
 * @brief Function implementing the ledTask thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_ledTaskFunc */
void ledTaskFunc(void const * argument) {
	/* USER CODE BEGIN ledTaskFunc */
	uint32_t distance = 0;
	osEvent eventMessage = { 0 };
	uint32_t freq = 1000; /* Czestotliwosc taktowania diody */
	uint32_t arr = freq - 1; /* Wartosc rejestru autoreload */
	uint32_t psc = ((SystemCoreClock / (arr + 1)) / freq) - 1; /* Wyliczenie preskalera dla timera */
	uint32_t ccr = 0; /* poczatkowa wartosc timera */
	uint32_t minDistance = 200; /* Minimalna wartosc odleglości dla diody 0% mocy */
	uint32_t maxDistance = 1000; /* Maksymalne wartość odleglości dla diody 100% mocy */
	uint32_t distanceSpread = maxDistance - minDistance; /* Rozpietosc odleglosci */
	float ledPower = 0.0; /* Wartość wyliczonej jasności diody w % */

	__HAL_TIM_SET_PRESCALER(&htim2, psc); /* Ustawienie reestru preskalera */
	__HAL_TIM_SET_AUTORELOAD(&htim2, arr); /* Ustawienie rejestru autoreload */

	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1); /* Start timera w trybie PWM swiecacego dioda */
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2); /* Start timera w trybie PWM swiecacego dioda */
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4); /* Start timera w trybie PWM swiecacego dioda */
	/* Infinite loop */
	for (;;) {
		eventMessage = osMessageGet(distQueueHandle, osWaitForever);
		if (osEventMessage == eventMessage.status) {
			distance = eventMessage.value.v;

			if (distance < minDistance) { /* Jesli wartość mniejsza niz minialna zgas diode 0% PWM */
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, 0); /* Minimalna wartosc wypelnienia wygnalu PWM */
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_2, 0); /* Minimalna wartosc wypelnienia wygnalu PWM */
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, 0); /* Minimalna wartosc wypelnienia wygnalu PWM */
				ledPowerGlobal = 0.0;
			} else if (distance > maxDistance) { /* Jeśli wartość wieksza niz maksymalna swiec diode 100% PWM */
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, arr + 1); /* Maksymalna wartosc wypelnienia */
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_2, arr + 1); /* Maksymalna wartosc wypelnienia */
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, arr + 1); /* Maksymalna wartosc wypelnienia */
				ledPowerGlobal = 100.0;
			} else { /* Jesli wartość miedzy min-max wylicz jasnosc diody */
				ledPower = (100.0 * (distance - minDistance)) / distanceSpread; /* Wylicza jasność diody w % */
				ccr = ((arr + 1) * ledPower) / 100.0;/* Przelicza wartosc w % na counter compare (wypenienie sygnalu PWM) */
				if (ccr < arr * 0.3)
					ccr = (arr + 1) * 0.3;
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, ccr); /* Ustawia wartość wypelnienia */
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_2, ccr); /* Ustawia wartość wypelnienia */
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, ccr); /* Ustawia wartość wypelnienia */
				ledPowerGlobal = ledPower;
			}
		}
	}
	/* USER CODE END ledTaskFunc */
}

/* USER CODE BEGIN Header_buttonTaskFunc */
/**
 * @brief Function implementing the buttonTask thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_buttonTaskFunc */
void buttonTaskFunc(void const * argument) {
	/* USER CODE BEGIN buttonTaskFunc */
	/* Infinite loop */
	for (;;) {
		osThreadSuspend(NULL);
		buttonGlobal++;
		osDelay(500);
		HAL_NVIC_EnableIRQ(EXTI15_10_IRQn); /* Włącza przerwania dla przycisku */
	}
	/* USER CODE END buttonTaskFunc */
}

/* USER CODE BEGIN Header_statTaskFunc */
/**
 * @brief Function implementing the statTask thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_statTaskFunc */
void statTaskFunc(void const * argument) {
	/* USER CODE BEGIN statTaskFunc */
	/* Infinite loop */
	for (;;) {
		statGlobal = triggerGlobal;
		triggerGlobal = 0;
		osDelay(1000);
	}
	/* USER CODE END statTaskFunc */
}

/**
 * @brief  Period elapsed callback in non blocking mode
 * @note   This function is called  when TIM6 interrupt took place, inside
 * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
 * a global variable "uwTick" used as application time base.
 * @param  htim : TIM handle
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	/* USER CODE BEGIN Callback 0 */

	/* USER CODE END Callback 0 */
	if (htim->Instance == TIM6) {
		HAL_IncTick();
	}
	/* USER CODE BEGIN Callback 1 */

	/* USER CODE END Callback 1 */
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
